
// create an array from a new class, it will arbitrary contain 100 objects
Rond [] rond = new Rond [100];

void setup () {
  size (500,500);
  frameRate(30);
  
  // fill the array, give objects values
  for (int i=0; i< rond.length; i++) {
    rond [i] = new Rond (random (width), random (height), random (width), random (height), color (random(255), random (255), random(255)));
  }
}

void draw () {
  background (255);
  
  // use the functions created further, using the dot operator
  // makes the draw section lighter (when there are less comments... :))
  for (int i=0; i< rond.length; i++) {
  rond[i].update();
  rond[i].edges();
  rond[i].display();
  }
  
 //collect images for the gif, each framecount and then stop the animation
 //obviously just need it once
 if (frameCount< 200) {
    saveFrame("data/animation-####.png");
} else {
    noLoop(); 
} 

// then i'll use FFmpeg to create a gif with the computer cmd, "outside" processing
// put the ffmpeg.exe at the data location
// then we can go at this location with the cd cmd and make our move !
// first create a palette of colours from one of the images with -palettegen, then
// re-collect the images created and the palette, fix a rate and convert into gif with
//ffmpeg -r 25 -i animation-%04d.png -i palette.png -filter_complex "paletteuse" animation.gif 
}

// create the class and all that goes in it
class Rond {
  PVector position;
  PVector velocity; 
  PVector acceleration; 
  PVector target;
  float maxspeed; 
  color rgb;
  
  // we could actually get rid of the color variable i think
  // as i fill with new ones in the display function but nevermind
  Rond (float x, float y,  float  xspeed, float yspeed, color rgb) {

  // "this" because same variable than the one declared just before
    this.rgb = rgb;  
  
  // create the vectors position and velocity, from the random values filled in setup
  position = new PVector (x,y);
  velocity = new PVector (xspeed, yspeed); 
  
  }
  
  void update () {
    // create a random target each frame, which will lure the objects
    // they kind of stick in the middle because
    // the random target are changing so fast they don't have the time
    // which fortunately still fits the homework goal : simulate collectiv bee motion 
    // (for real flocking i may later create an orbit or pattern for the new targets?)
    
      target = new PVector (random(width), random(height)); 
      
   // the lure is just the difference to fill btw the target and the position 
      PVector lure = PVector.sub(target,position);
      
   // we scale it so that we don't have crazy values, but we keep the new direction
    lure.normalize();
    lure.mult(0.5);  
    
    // could have kept "lure" but still struggling with the physics behind 
    // so i'll keep the more conventional word
    acceleration = lure; 
    
    // to avoid crazy values
    maxspeed = 3;
    
    // here are the physics !
    // velocity is corrected and transformed by acceleration at each frame
    // and acceleration = lure, so it's the influence of the target on the objects
    // and then we change the position vector by adding to it this new velocity vector
    // after what we shall reinitialize the acceleration -i just follow an advice here
  velocity.add(acceleration);   
  velocity.limit(maxspeed);
  position.add(velocity);
  acceleration.mult(0);    
 
} 

// this edges funktion is kind of useless in the end i think
// it is supposed to avoid objects going out of the window
// but as the target keep the objects in the middle... it is probably never used
// but hey we wrote it, let's keep it for now
  void edges () {
    if (position.x<0 || position.x > width) {
     velocity.x *= -1;
    }
    if (position.y<0 || position.y > height) {
      velocity.y *= -1;
    }
}
 
 // display funktion just draws the object with the right color and position       
  void display () {
    
    //fill (rgb); => in the end I prefer continuous color change
    fill (random(255), random (255), random (255));
    ellipse (position.x, position.y, 50, 50);
    
    //ellipse (target.x, target.y, 10,10); => used to see all the crazy random targets
    // and to actually understand what was really going on
  }
  
}


  
